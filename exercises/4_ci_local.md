# Exercise - Continuous Integration (local)

## Update environment
To ensure certain aspects of code quality, some new packages have been added to our requirements.
Make sure you have an up-to-date environent:
```
conda env create -f environment.yml --force
```
If you are using venv instead of conda, you can do:
```
pip install -r requirements.txt
```
Now a few new packages have been installed, to understand how they work please read [this](../README.md#ensuring-consistent-code-quality-checks).
Make sure pre-commit is enabled in your repo:
```
pre-commit install
pre-commit run --all-files
```

## Code quality complaints
After doing all of the above try to make a bad commit, for example by removing the docstring of a function. Read the git output and make sure that there is a flake8 complaint similar to:
```
deployment/score_v1.py:9:1: D103 Missing docstring in public function
```
