# Exercise - Continuous Integration (gitlab)

Now we want gitlab to check pre-commit every time a change is pushed to gitlab:

- Find out how to create a typical pipeline for Python projects in gitlab
- Create .gitlab-ci.yml based on your findings and add a step called `lint` which runs the following command:
```
pip install pre-commit==2.17.0
pre-commit run --all-files
```
- Commit and push your changes and check whether the pipeline is running correctly in gitlab
- (Optional) add cache folders for pip and pre-commit to reduce reduntant work
