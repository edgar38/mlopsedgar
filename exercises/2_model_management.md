# Exercise - Model management
*When done with training runs, you would like your best model run to be registered as a (new) version in AML.*

You can use model registration to store and version your models in your workspace. Registered models are identified by name and version. Each time you register a model with the same name as an existing one, the registry increments the version. 

1. Add a tag “ready_for_reg” with a boolean value to your model training file so that this tag will be logged during training runs.
2. Next add some mlflow code in the logging of your model, so that the model will be registered when your tag is equal to True. Additionaly add an extra pip requirements file during logging ("extra_deploy_requirements.txt"). At last make sure that your artifact_path is "model".
2. Pick an experiment run with the best results and run the training, using the best parameters, and using the boolean “ready_for_reg” to True in order to register the model.
