# Exercise - Model deployment
In order to deploy and run the model as an endpoint, the model needs to be packaged in an inference environment.

This model inference will make sure that the input requests will be handled correctly and result in output with model outcomes.

Please create a new file including the model inference and deployment code. Start with copying the content of 'code/aml_utils' into this new file.

## Model artifacts
Download the most recent registered model artifacts to your local repository by copying the following snippets in your file:
```python
from azureml.core.model import Model

source_directory = "."
model = Model(ws, os.environ["MODEL_NAME"])
model.download(target_dir=source_directory, exist_ok=True)
```

## Model scoring file
In order to use the input of an input request and let the model score the output, a (new) scoring file needs to be created including the following code:
```python
import json

def init():
    """Load model"""
    print("This is init")

def run(data):
    """Run on each request"""
    test = json.loads(data)
    print(f"received data {test}")
    return f"test is {test}"
```

## Model inference
Continue with your inference and add the following code to create an environment for your model inference:
```python
from azureml.core.model import InferenceConfig
from azureml.core import Environment

env = Environment.from_conda_specification(
    name="model_environment", file_path=f"{source_directory}/model/conda.yaml"
)
env.docker.base_image = "mcr.microsoft.com/azureml/openmpi3.1.2-cuda10.1-cudnn7-ubuntu18.04"
```
Load your newly created scoring file and define the inference config:
```python
entry_script = "<YOUR SCORING FILEPATH>"
inference_config = InferenceConfig(
    environment=env,
    source_directory=source_directory,
    entry_script=entry_script,
)
```

## Model deployment
Add the following lines to be able to deploy your newly created endpoint locally:
```python
from azureml.core.webservice import LocalWebservice

deployment_config = LocalWebservice.deploy_configuration(port=6789)

service = Model.deploy(
    workspace=ws,
    name=os.environ["SERVICE_NAME"],
    models=[model],
    inference_config=inference_config,
    deployment_config=deployment_config,
    overwrite=True,
)
service.wait_for_deployment(show_output=True)

print(service.get_logs())
print(f"Scoring URI is : {service.scoring_uri}")
```

## Endpoint usage
For validation of local endpoint deployment use the following code (preferably in another file):
```python
import json

import requests
from sklearn import datasets
from sklearn.model_selection import train_test_split

scoring_uri = "http://0.0.0.0:6789/score"

input_data = '{"data": "test"}'
headers = {"Content-Type": "application/json"}
resp = requests.post(scoring_uri, json.dumps(input_data), headers=headers)

print("POST to url", scoring_uri)
print("Prediction endpoint:", resp.text)
```

## Refine scoring file 
*Exercise:* Go ahead and change the scoring file in order to load your model and get predictions in the run section.

You can debug your file locally using the "debug locally" section below.

Validate your new scoring file after debugging by deploying as a local endpoint. As shown above.

## Debug locally
You are also able to validate your scoring file without inference by using the following terminal command (more info: https://docs.microsoft.com/en-us/azure/machine-learning/how-to-troubleshoot-deployment?tabs=azcli#azure-machine-learning-inference-http-server):
```
azmlinfsrv --entry_script <YOUR SCORING FILEPATH>
```
Send a scoring request to the server using curl:
```
curl -p 127.0.0.1:5001/score
```

## Deploy to cloud compute
When your endpoint service is debugged and validated, you can deploy to an Azure Cloud Instance (ACI).

Adjust your inference deployment by changing the definition of the deployment_config variable to an ACI deployment instead of local config. Make sure the ACI config includes the following requirements:
* Number of CPU cores: 0.5
* Amount of memory: 1 GB

After running the code of "model deployment" with the ACI deployment config, you are able to test your endpoint again.
