# %%
import os

from azureml.core import Environment, Workspace, authentication
from azureml.core.model import InferenceConfig, Model
from azureml.core.webservice import AciWebservice
from dotenv import load_dotenv

load_dotenv()

# User authentication
interactive_auth = authentication.InteractiveLoginAuthentication(tenant_id=os.environ["TENANT_ID"])

# Get workspace environment
ws = Workspace.get(
    name=os.environ["WORKSPACE_NAME"],
    auth=interactive_auth,
    subscription_id=os.environ["SUBSCRIPTION_ID"],
    resource_group=os.environ["RESOURCE_GROUP"],
)

source_directory = "."
model = Model(ws, os.environ["MODEL_NAME"])
model.download(target_dir=source_directory, exist_ok=True)

env = Environment.from_conda_specification(
    name="model_environment", file_path=f"{source_directory}/model/conda.yaml"
)
env.docker.base_image = "mcr.microsoft.com/azureml/openmpi3.1.2-cuda10.1-cudnn7-ubuntu18.04"

entry_script = "score.py"
inference_config = InferenceConfig(
    environment=env,
    source_directory=source_directory,
    entry_script=entry_script,
)
deployment_config = AciWebservice.deploy_configuration(cpu_cores=0.5, memory_gb=1)

# %% Deployment to ACI
service = Model.deploy(
    workspace=ws,
    name=os.environ["SERVICE_NAME"],
    models=[model],
    inference_config=inference_config,
    deployment_config=deployment_config,
    overwrite=True,
)
service.wait_for_deployment(show_output=True)

print(service.get_logs())
print(f"Scoring URI is : {service.scoring_uri}")

# %%
