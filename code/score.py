import json
import os
import pickle

import pandas as pd

# Debug score.py locally - "https://docs.microsoft.com/en-us/azure/machine-learning/how-to-troubleshoot-deployment?tabs=azcli#azure-machine-learning-inference-http-server"


def init():
    """Init when service is starting"""
    global model
    with open(
        os.path.join(os.getenv("AZUREML_MODEL_DIR", "."), "model/model.pkl"), "rb"
    ) as model_file:
        model = pickle.load(model_file)
    print("This is init")


def run(data):
    """Performed with each request"""
    print(f"+++++++++++ data: {data}")
    input_data = json.loads(data)
    print(f"+++++++++++ received data: {input_data}")
    prediction = model.predict(pd.DataFrame(input_data))
    print(f"+++++++++++ prediction: {prediction}")
    return f"Prediction: {prediction}"
